package facci.aymemacias.miprimeraapp;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText txtClassFahrenheit;
    EditText txtClassCentigrados;
    Button btnConversionCentigrados;
    Button btnConversionFahrenheit;
    Button btnBorrado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("Mi_Primer_Proyecto", "Jeckye Ayme Macías Saldarriaga");
        btnConversionCentigrados = (Button) findViewById(R.id.btnConversionCent);
        btnConversionFahrenheit = (Button) findViewById(R.id.btnConversionFah);
        btnBorrado = (Button) findViewById(R.id.btnBorrar);
        txtClassFahrenheit = (EditText) findViewById(R.id.txtFahrenheit);
        txtClassCentigrados = (EditText) findViewById(R.id.txtCentigrados);

        btnConversionCentigrados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float valor = Float.parseFloat(txtClassFahrenheit.getText().toString());
                txtClassCentigrados.setText(String.valueOf(ConverterUtil.convertFahrenheit_Celsius(valor)));

            }
        });
        btnConversionFahrenheit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float valor2 = Float.parseFloat(txtClassCentigrados.getText().toString());
                txtClassFahrenheit.setText(String.valueOf(ConverterUtil.convertCelsius_Fahrenheit(valor2)));
            }
        });
        btnBorrado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtClassFahrenheit.setText("");
                txtClassCentigrados.setText("");
            }
        });
    }

    public static class ConverterUtil {
        public static float convertFahrenheit_Celsius(float fahrenheit) {
            return ((fahrenheit - 32) * 5 / 9);
        }
        public static float convertCelsius_Fahrenheit(float celsius) {
            return ((celsius * 9) / 5) + 32;
        }
    }
}